#include <iostream>
#include "static_mat.h"
#include <unistd.h>
using namespace rp;
using namespace std;

using Vec3f=Vec_<float, 3>;
using Mat3f=Mat_<float, 3,3>;
using Mat3_4f=Mat_<float, 3,4>;

int main() {
Mat_<float, 3,2> A;
randFill(A);
cout << "A:" << A << endl;

Mat_<float, 2,3> B;
randFill(B);
cout << "B:" << B << endl;

Mat_<float, 3,3> C;
randFill(C);
cout << "C:" << C << endl ;

Mat_<float, 3,1> v;
randFill(v);
cout << "v:" << v << endl;

Mat_<float, 3,3> res;
  for (int i=0; i<1000000; ++i) {
    res=C*(A*B+v*v.transpose());
    }
  cout << res;
  // TODO FILL_HERE_THE_STUFF_FOR_EXERCISE_1
}
